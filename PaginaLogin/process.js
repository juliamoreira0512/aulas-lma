const {createApp} = Vue;

createApp({
    data(){
        return{
            username: '',
            password: '',
            error: null,
            right: null,

            //arrays para armazenamento dos usuarios e senhas
            usuarios: ['admin', 'thayne'],
            senhas: ['1234', '1234'],

            //variaveis para armazenamento do usuario e senha que será cadastrado
            newUsername: "",
            newPassWord: "",
            confirmPassword: "",

            //Interação das mensagens de texto
            mostrarEntrada: false,

            mostrarLista: false,
        } //Fechamento return
    }, //Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                if((this.username === 'thayne' && this.password === '1234') || 
                (this.username === 'Joseana' && this.password === 'Mamaelinda')){
                    this.right = "Login Realizado com Sucesso!";
                    this.error = null;
                } //Fechamento do if
                else{
                    this.error = "Nome ou senha incorretos!";
                    this.right = null;
                } //Fim do else
            }, 1000);
        }, //Fechamento login

        loginArray(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if

            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //uso de uma constante para armazenar o número do index onde o usuário "procurando" está "guardando" no array
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.right = "Login efetuado com sucesso!";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                }// Fechamento if 
                else{
                    this.right = null;
                    this.error = "Usuário ou senha incorretos!";
                } // Fechamento do else

                this.username = "";
                this.password = "";


            }, 500);

        }, //Fechamento loginArray

        adicionarUsuario(){
            this.mostrarEntrada = false;

            //update no conteúdo dos arrays para recuperar os dados armazenados no local storage
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            
            setTimeout(() => {
                this.mostrarEntrada = true;
                if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                    if(this.newPassWord && this.newPassWord === this.confirmPassword){
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassWord);

                        //Atualizando os valores dos arrays no armazenamento local
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                        this.newUsername = "";
                        this.newPassWord = "";
                        this.confirmPassword = "";
                        this.right = "Usuário cadastrado com sucesso";
                        this.error = null;
                    }//Fechamento if
                    else{
                        this.error = "Por favor, digite uma senha válida!";
                        this.right = null;
                    }//Fechamento do else
                }//Fechamento if
                else{
                    this.error = "Por favor, informe um usuario válido!";
                    this.right = null;
                }//Fechamento else
        }, 500);
        },//Fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento verCadastrados

        paginaCadastro(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                this.right = "Carregando página de cadastro!";
                this.error = null;
            }, 500);

            setTimeout(() => {
                window.location.href = "cadastro.html";
            }, 2000);

        },//Fechamento da pagina Cadastro 

    }, //Fechamento methods
}).mount("#app"); //Fechamento createApp
