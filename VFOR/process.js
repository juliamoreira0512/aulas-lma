const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens: ["Bola", "Bolsa", "Tenis"],

        };//Fechamento return
    },//Fechamento data

    methods:{
        showItens: function(){
            this.show = !this.show;
        },//Fechamento show itens
    },//Fechamento methods

}).mount("#app");//Fechamento createApp