const {createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://i.pinimg.com/736x/3d/74/b1/3d74b17105ce1d2046935a29331cc55e.jpg',
                'https://i.pinimg.com/474x/7e/aa/d4/7eaad4fd48b806a6c8813dd0d775e526.jpg',
                'https://img.freepik.com/vetores-premium/ilustracao-de-desenho-animado-de-ukulele_286786-1265.jpg?w=2000'
            ],

        };//Fim return
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },//Fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        },//Fim randomImageInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//Fim methods

}).mount("#app");